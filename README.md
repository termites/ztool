# 入门和安装

<div align=center><img src="https://kuaimashidai.oss-cn-beijing.aliyuncs.com/image/logo.png" width="  "></div>

<center style="font-weight: bold">ZTool为了让Go开发更快捷而生</center>
<center>
<a href="https://github.com/druidcaesa/ztool"> github仓库地址</a>
<a href="https://gitee.com/termites/ztool">gitee码云仓库地址</a>
</center>

# 语言：简体中文  [English](README-en.md)

## 一、简介

**ZTool是一个小巧而快捷工具库，通过方法封装，降低相关API的学习成本，提高工作效率，让使用GO语言开发编写代码更加优雅。**

**ZTool中的工具方法来自我们长达两年的多使用go语言进行实际项目开发总结而来，它涵盖了GO开发中常用的方法，它既是大型项目开发中解决小问题的利器，也是小型项目中的效率担当；**

**ZTool它节省了开发人员对项目中公用类和公用工具方法的封装时间，使开发专注于业务，同时可以最大限度的避免封装不完善带来的bug。**

## 二、安装

### 1、go环境准备

1. go开发环境安装

* 安装包下载地址：[官方地址:https://go.dev/dl/](https://go.dev/dl/)

* 如果无法打开请使用备选地址：[备选地址：https://go.dev/dl/](https://go.dev/dl/)

2. go env来查看和验证go的环境信息，例如我的部分配置信息如下：

```shell
GO111MODULE="on"
GOARCH="amd64"
GOBIN=""
GOCACHE="/Users/Janus/Library/Caches/go-build"
GOENV="/Users/Janus/Library/Application Support/go/env"
GOMODCACHE="/Users/Janus/gopath/pkg/mod"
GONOPROXY="github.com"
GONOSUMDB="github.com"
GOPATH="/Users/Janus/gopath"
GOPRIVATE="github.com"
GOPROXY="https://goproxy.cn,direct"
GOROOT="/usr/local/go"
GOSUMDB="sum.golang.org"
GOTOOLDIR="/usr/local/go/pkg/tool/darwin_amd64"
```

<font color="red">注意：GO111MODULE必须是开启开启状态,若为开启请自行google或百度解决</font>

### 2、ztool工具包安装

1. 版本要求

```go
go >=1.15 
```

2. 版本检查

```go
go version 
```

3. go get 安装

```shell
go get -u -b github.com/druidcaesa/ztool
```

4. go mod安装

```shell
require github.com/druidcaesa/ztool
```

## 三、添砖加瓦

### 1、分支说明

|分支|说明|
|:----:|:----:|
|master|主分支，release的版本使用分支，该仓库不接受任何的pr获取修改|
|v1-dev|版本v1开发分支，该版本接受修改或者pr|

### 2、BUG修复或这件反馈渠道

* [Gitee](https://gitee.com/termites/ztool/issues)
* [GitHub](https://github.com/druidcaesa/ztool/issues)

### 3、怎么贡献代码呢

1. 在Gitee或者Github上fork项目到自己的repo
2. 把fork过去的项目也就是你的项目clone到你的本地
3. 修改代码（记得一定要修改v1-dev分支）
4. commit后push到自己的库（v1-dev分支）
5. 登录Gitee或Github在你首页可以看到一个 pull request 按钮，点击它，填写一些说明信息，然后提交即可。
6. 等待维护者合并

### 4、PR需要遵循的原则

> ZTool欢迎任何人为ZTool测成长舔砖加瓦，贡献自己的代码，不过为了项目的整体阅读性和维护行，需要符合一下这些规范，规范如下：

1. 注释完备，尤其每个新增的方法应按照Go文档规范标明方法说明、参数说明、返回值说明等信息，并在对应测试文件中添加测试方法，如果愿意，也可以加上你的大名。
2. 新添加的方法亲不要做第三方库的依赖加入，ZTool为了减少对其他库的依赖，所以进制第三方库引入
3. 请pull request到v1-dev分支中，master是主分支，打tag都是从master分支中打出来的，master不允许做任何修改，pr的所有请求将直接关闭

# 四、有关详细信息，请参阅文档

* 文件地址 [文档地址](https://termites.gitee.io/zbook-blog)