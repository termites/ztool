<div align=center><img src="https://kuaimashidai.oss-cn-beijing.aliyuncs.com/image/logo.png" width="  "></div>

<center style="font-weight: bold">Z Tool was born to make Go development faster</center>

<center>
<a href="https://github.com/druidcaesa/ztool">  github repository address</a>
<a href="https://gitee.com/termites/ztool"> gitee code cloud warehouse address</a>
</center>

# Language：English  [简体中文](README-zh.md)
## 一、Introduction

**ZTool It is a small and fast tool library. Through method encapsulation, it reduces the learning cost of related APIs, improves work efficiency, and makes developing and writing code in GO language more elegant.**

**The tools and methods in ZTool come from our two-year-long summary of using the go language for actual project development. It covers the methods commonly used in GO development. It is not only a tool to solve small problems in the development of large-scale projects, but also a small project. responsibility for efficiency in**

**ZTool saves developers the time to encapsulate common classes and common tool methods in the project, makes development focus on business, and can avoid bugs caused by imperfect encapsulation to the greatest extent.**

## 二、Introduction

### 1、go environment preparation

1. go development environment installation

* Installation package download address：[official address:https://go.dev/dl/](https://go.dev/dl/)

* If it cannot be opened, please use an alternate address：[Alternative address：https://go.dev/dl/](https://go.dev/dl/)

2. go env To view and verify the environment information of go, for example, some of my configuration information is as follows：

```shell
GO111MODULE="on"
GOARCH="amd64"
GOBIN=""
GOCACHE="/Users/Janus/Library/Caches/go-build"
GOENV="/Users/Janus/Library/Application Support/go/env"
GOMODCACHE="/Users/Janus/gopath/pkg/mod"
GONOPROXY="github.com"
GONOSUMDB="github.com"
GOPATH="/Users/Janus/gopath"
GOPRIVATE="github.com"
GOPROXY="https://goproxy.cn,direct"
GOROOT="/usr/local/go"
GOSUMDB="sum.golang.org"
GOTOOLDIR="/usr/local/go/pkg/tool/darwin_amd64"
```

<font color="red">Note: GO 111 MODULE must be turned on, if it is turned on, please google or Baidu to solve it</font>

### 2、ztool toolkit installation

1. Version requirements

```go
go >=1.15 
```

2. version check

```go
go version 
```

3. go get Install

```shell
go get -u -b github.com/druidcaesa/ztool
```

4. go mod Install

```shell
require github.com/druidcaesa/ztool
```

## 三、Add bricks and mortar

### 1、branch description

|branch|illustrate|
|:----:|:----:|
|master|The main branch, the release version uses the branch, the repository does not accept any pr fetch modifications|
|v1-dev|Version v 1 development branch, this version accepts modification or pr|

### 2、Bug fix or this piece of feedback channel

* [Gitee](https://gitee.com/termites/ztool/issues)
* [GitHub](https://github.com/druidcaesa/ztool/issues)

### 3、How to contribute code

1. Fork the project to your own repo on Gitee or Github
2. Clone the fork past project that is your project to your local
3. Modify the code (remember to modify the v 1-dev branch)
4. After commit, push to your own library (v 1-dev branch)
5. Log in to Gitee or Github to see a pull request button on your homepage, click it, fill in some description information, and submit it.
6. Waiting for the maintainer to merge

### 4、PR principles to follow

> ZTool welcomes anyone to measure the growth of Z Tool and contribute their own code, but for the overall readability and maintenance of the project, it is necessary to comply with these specifications. The specifications are as follows:

1. The annotations are complete, especially each new method should be marked with method description, parameter description, return value description and other information according to the Go document specification, and add the test method in the corresponding test file, if you like, you can also add your name.
2. For the newly added method, please do not add dependencies to third-party libraries. In order to reduce dependencies on other libraries, Z Tool is introduced into third-party libraries.
3. Please pull request to the v1-dev branch, the master is the main branch, and all tags are typed from the master branch. The master does not allow any modification, and all requests from pr will be closed directly.

# 四、See the documentation for details

* file address [address](https://termites.gitee.io/zbook-blog)